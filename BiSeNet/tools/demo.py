import sys

from tqdm import tqdm

sys.path.insert(0, '.')
import argparse
import math
import torch
import torch.nn as nn
import torch.nn.functional as F
from PIL import Image
import numpy as np
import cv2
import os
import lib.data.transform_cv2 as T
from lib.models import model_factory
from configs import set_cfg_from_file

# uncomment the following line if you want to reduce cpu usage, see issue #231
# torch.set_num_threads(4)

torch.set_grad_enabled(False)
np.random.seed(123)

# args
parse = argparse.ArgumentParser()
parse.add_argument('--config', dest='config', type=str, default='configs/bisenet_customer.py', )
parse.add_argument('--weight-path', type=str, default='./res/model_segmentation.pth', )
parse.add_argument('--img-path', dest='img_path', type=str, required=True)
args = parse.parse_args()
cfg = set_cfg_from_file(args.config)

palette = np.random.randint(0, 256, (256, 1), dtype=np.uint8)

# define model
net = model_factory[cfg.model_type](cfg.n_cats, aux_mode='eval')
net.load_state_dict(torch.load(args.weight_path, map_location='cpu'), strict=False)
net.eval()
# net.cuda()
# prepare data
to_tensor = T.ToTensor(
    mean=cfg.mean,
    std=cfg.std,
)

path = os.path.join(args.img_path, 'JPEGImages')
imgviz_path = os.path.join(args.img_path)
fps = 60  # 视频每秒24帧
size = (640, 480)  # 需要转为视频的图片的尺寸
# 可以使用cv2.resize()进行修改
fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
video = cv2.VideoWriter(os.path.join(imgviz_path, "BiSeNetVis.mp4"), fourcc, fps, size)  # 创建.mp4
imgs = os.listdir(path)
imgs.sort()
for img_p in tqdm(imgs):
    img_path = os.path.join(path, img_p)
    im = cv2.imread(img_path)[:, :, ::-1]
    im = to_tensor(dict(im=im, lb=None))['im'].unsqueeze(0)

    # shape divisor
    org_size = im.size()[2:]
    new_size = [math.ceil(el / 32) * 32 for el in im.size()[2:]]
    import time

    st_time = time.time()
    # inference
    im = F.interpolate(im, size=new_size, align_corners=False, mode='bilinear')
    out = net(im)[0]
    out = F.interpolate(out, size=org_size, align_corners=False, mode='bilinear')
    out = out.argmax(dim=1)

    out = out.squeeze().detach().cpu().numpy()
    pred = palette[out]

    import imgviz

    img = cv2.imread(img_path)[:, :, ::-1]
    viz = imgviz.label2rgb(
        label=pred.reshape(480, 640),
        image=img,
    )
    viz = viz[:, :, ::-1]
    video.write(viz)

video.release()
