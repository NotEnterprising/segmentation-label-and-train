## bisenetv2 cityscapes
export CUDA_VISIBLE_DEVICES=0
cfg_file=configs/bisenet_customer.py
NGPUS=1
torchrun --nproc_per_node=$NGPUS tools/train_amp.py --config $cfg_file