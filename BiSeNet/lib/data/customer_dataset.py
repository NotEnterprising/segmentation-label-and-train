#!/usr/bin/python
# -*- encoding: utf-8 -*-
import numpy as np

import lib.data.transform_cv2 as T
from lib.data.base_dataset import BaseDataset


class CustomerDataset(BaseDataset):

    def __init__(self, dataroot, annpath, mean, std, trans_func=None, mode='train'):
        super(CustomerDataset, self).__init__(
            dataroot, annpath, trans_func, mode)
        self.n_cats = 4
        self.lb_ignore = 255
        self.lb_map = np.arange(256).astype(np.uint8)

        self.to_tensor = T.ToTensor(
            mean=mean,
            std=std,
        )
