# Segmentation Label and Train

## Installation
* Create conda environment
```angular2html
conda create -n pose_estimation_seg python=3.8
conda activate pose_estimation_seg
```
* Install pytorch and torchvision [pytorch](https://pytorch.org/get-started/locally/)
```angular2html
pip install torch==1.10.1+cu111 torchvision==0.11.2+cu111 torchaudio==0.10.1 -f https://download.pytorch.org/whl/torch_stable.html
```
* Install requirements python packages
```angular2html
pip install labelme opencv-python-headless tqdm tabulate
```

## How to Use
### Add new object label
* Modify STM/labels.txt 
### Rosbag file extract
```angular2html
python STM/tools/bag2dir.py [bag_file_path]
```
### Run
```angular2html
bash run.sh [imgs_input_dir] [output_dir_name] [object_type]
# examples : bash run.sh data/6inch_cassette_on_agv_upright_rmse_0.009_ruihong_yellow_light_real/color test cassette
```
### Output structure
```angular2html
output
   ├── 6inch_cassette_on_machine_ruihong_yellow_light_real
   │   ├── BiSeNetVis.mp4 (BiSeNet visualize video)
   │   ├── class_names.txt (Labels list)
   │   ├── JPEGImages (RGB images)
   │   ├── SegmentationClassPNG (STM label images)
   │   ├── SegmentationClassVisualization (STM visualize images)
   │   ├── STMOutputImages (STM output images)
   │   └── vis.mp4 (STM visualize video)
   ├── 6inch_loadport_on_table_ruihong_yellow_light_real
   │   ├── BiSeNetVis.mp4
   │   ├── class_names.txt
   │   ├── JPEGImages
   │   ├── SegmentationClassPNG
   │   ├── SegmentationClassVisualization
   │   ├── STMOutputImages
   │   └── vis.mp4
   ├── train.txt
   └── val.txt
```
## Reference

``` 
Video Object Segmentation using Space-Time Memory Networks
Seoung Wug Oh, Joon-Young Lee, Ning Xu, Seon Joo Kim
ICCV 2019
```

[[paper]](https://openaccess.thecvf.com/content_ICCV_2019/html/Oh_Video_Object_Segmentation_Using_Space-Time_Memory_Networks_ICCV_2019_paper.html)
[[github]](https://github.com/seoungwugoh/STM)

``` 
Fast Video Object Segmentation by Reference-Guided Mask Propagation
Seoung Wug Oh, Joon-Young Lee, Kalyan Sunkavalli, Seon Joo Kim
CVPR 2018
```

[[paper]](http://openaccess.thecvf.com/content_cvpr_2018/papers/Oh_Fast_Video_Object_CVPR_2018_paper.pdf)
[[github]](https://github.com/seoungwugoh/RGMP)

```
BiSeNet: Bilateral Segmentation Network for Real-time Semantic Segmentation
Changqian Yu, Jingbo Wang, Chao Peng, Changxin Gao, Gang Yu, Nong Sang
ECCV 2018
```
[[paper]](https://arxiv.org/abs/1808.00897)
[[github]](https://github.com/CoinCheung/BiSeNet)