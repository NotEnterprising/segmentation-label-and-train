#!/bin/bash
# input arguments
input_dir=$1
output_dir=$2
object=$3

# Enter the STM path and read the labels
echo 'Enter the STM path and read the labels'
value=$(cat STM/labels.txt)
labels=""
for line in $value; do
  labels="$labels,$line"
done
labels=${labels:1}

# Convert the images of the input path to STM format
echo 'Convert the images of the input path to STM format'
python STM/tools/labelme2voc.py images_to_stm ${input_dir} ./STM/output/${output_dir}

# Enter STM/output/${output_dir}/JPEGImages
echo 'Enter STM/output/${output_dir}/JPEGImages'
cd STM/output/${output_dir}/JPEGImages

# Get the images under the current path and label one every 500 images
echo 'Get the images under the current path and label one every 500 images'
for filename in $(ls ./); do
  str=${filename:0:5}
  num=$(expr $str + 0)
  mod=$(expr $num % 500)
  path="../SegmentationClassPNG/$str.json"
  if [ $mod -eq 0 ]; then
    labelme $filename --labels $labels -O $path
  fi
done

# Back to the STM path
echo 'Back to the STM path'
cd ../../..

# Convert json annotation file to png format
echo 'Convert json annotation file to png format'
python tools/labelme2voc.py json_to_png ./output/${output_dir}

# Tracking annotated image sequences with STM
echo 'Tracking annotated image sequences with STM'
python eval.py -d ./output/${output_dir} -o $object

# Split training set and test set based on STM output
echo 'Split training set and test set based on STM output'
python gen_train_file.py ./output

# Enter BiSeNet path
echo 'Enter BiSeNet path'
cd ../BiSeNet

# Check image format and calculate mean and std
echo 'Check image format and calculate mean and std'
python tools/check_dataset_info.py --im_root ../STM --im_anns ../STM/output/train.txt

# train segmentation network
echo 'train segmentation network'
bash train.sh

# visualize the segment result
python tools/demo.py --img-path ../STM/output/${output_dir}