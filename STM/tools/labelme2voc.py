#!/usr/bin/env python
from __future__ import print_function
import argparse
import glob
import os
import os.path as osp
import sys
import labelme
from PIL import Image
import numpy as np

labels_file = ['__ignore__',
               '_background_']


def ImagesToSTM(args):
    if args.image_type not in ['jpg', 'png']:
        print('input images type should be (jpg or png)')
        return
    if osp.exists(args.output_dir):
        os.system('rm -r {}'.format(args.output_dir))
    os.makedirs(args.output_dir)
    os.makedirs(osp.join(args.output_dir, "JPEGImages"))
    os.makedirs(osp.join(args.output_dir, "SegmentationClassPNG"))
    os.makedirs(osp.join(args.output_dir, "SegmentationClassVisualization"))
    print("Creating dataset:", args.output_dir)

    imgs_output_path = osp.join(args.output_dir, "JPEGImages")
    imgs = glob.glob(osp.join(args.input_dir, "*." + str(args.image_type)))
    count = 0
    imgs.sort()
    name2frame = {}
    for index, img in enumerate(imgs):
        base = osp.splitext(osp.basename(img))[0]
        save_path = os.path.join(imgs_output_path, '{:05d}.png'.format(count))
        name2frame[base] = '{:05d}'.format(count)
        os.system('cp {} {}'.format(img, save_path))
        count += 1


def JsonToPng(args):
    class_names = []
    class_name_to_id = {}
    file = open('./labels.txt', 'r')
    for line in file:
        content = line.replace('\n', '')
        if len(content) > 0:
            labels_file.append(content)
    for i, line in enumerate(labels_file):
        class_id = i - 1  # starts with -1
        class_name = line.strip()
        class_name_to_id[class_name] = class_id
        if class_id == -1:
            assert class_name == "__ignore__"
            continue
        elif class_id == 0:
            assert class_name == "_background_"
        class_names.append(class_name)
    class_names = tuple(class_names)
    print("class_names:", class_names)
    out_class_names_file = osp.join(args.output_dir, "class_names.txt")
    with open(out_class_names_file, "w") as f:
        f.writelines("\n".join(class_names))
    print("Saved class_names:", out_class_names_file)
    for filename in glob.glob(osp.join(args.output_dir, "SegmentationClassPNG", "*.json")):
        print("Generating dataset from:", filename)

        label_file = labelme.LabelFile(filename=filename)

        base = osp.splitext(osp.basename(filename))[0]
        out_png_file = osp.join(
            args.output_dir, "SegmentationClassPNG", base + ".png"
        )
        img = labelme.utils.img_data_to_arr(label_file.imageData)

        lbl, _ = labelme.utils.shapes_to_label(
            img_shape=img.shape,
            shapes=label_file.shapes,
            label_name_to_value=class_name_to_id,
        )
        img = Image.fromarray(lbl.astype(np.uint8), mode="P")
        img.save(out_png_file)
        # labelme.utils.lblsave(out_png_file, lbl)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    sub_parser = parser.add_subparsers()

    # ===================from bag====================
    parser_dataset = sub_parser.add_parser("images_to_stm")
    parser_dataset.add_argument("input_dir", help="input annotated directory")
    parser_dataset.add_argument("output_dir", help="output dataset directory")
    parser_dataset.add_argument("--image_type", help="input images type should be (jpg or png)", default='png')
    parser_dataset.set_defaults(func=ImagesToSTM)

    # ===================bag2dir====================
    parser_dataset = sub_parser.add_parser("json_to_png")
    parser_dataset.add_argument("output_dir", help="input annotated directory")
    parser_dataset.set_defaults(func=JsonToPng)

    args = parser.parse_args()
    args.func(args)
