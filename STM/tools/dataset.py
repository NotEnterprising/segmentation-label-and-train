import os
import os.path as osp

import cv2
import numpy as np
from PIL import Image

import torch
import torchvision
from torch.utils import data

import glob


class DAVIS_MO_Test(data.Dataset):
    # for multi object, do shuffling

    def __init__(self, root, single_object=True, lowerbound=0, upperbound=255):
        self.root = root
        self.mask_dir = os.path.join(root, 'SegmentationClassPNG')
        self.image_dir = os.path.join(root, 'JPEGImages')

        self.videos = []
        self.num_frames = {}
        self.num_objects = {}
        self.shape = {}
        self.size_480p = {}
        self.index_map = {}
        self.lowerbound = lowerbound
        self.upperbound = upperbound
        num_frames_total = len(glob.glob(os.path.join(root, 'JPEGImages', '*.png')))
        labels_path = glob.glob(os.path.join(root, 'SegmentationClassPNG', '*.png'))
        labels = []
        for label in labels_path:
            labels.append(osp.splitext(osp.basename(label))[0])
        labels.sort()
        divide_list = [int(label.split('.')[0]) for label in labels]
        divide_list.append(num_frames_total)
        for index in range(len(divide_list) - 1):
            start = divide_list[index]
            end = divide_list[index + 1]
            _video = 'frame' + str(start) + '-' + str(end - 1)
            self.videos.append(_video)
            self.num_frames[_video] = end - start
            _mask = np.array(Image.open(os.path.join(self.mask_dir, '{:05d}.png'.format(start))).convert("P"))
            self.num_objects[_video] = 1
            self.shape[_video] = np.shape(_mask)
            self.index_map[_video] = (start, end)
        self.K = 11
        self.single_object = single_object

    def __len__(self):
        return len(self.videos)

    def To_onehot(self, mask):
        M = np.zeros((self.K, mask.shape[0], mask.shape[1]), dtype=np.uint8)
        for k in range(self.K):
            M[k] = (mask == k).astype(np.uint8)
        return M

    def All_to_onehot(self, masks):
        Ms = np.zeros((self.K, masks.shape[0], masks.shape[1], masks.shape[2]), dtype=np.uint8)
        for n in range(masks.shape[0]):
            Ms[:, n] = self.To_onehot(masks[n])
        return Ms

    def __getitem__(self, index):
        video = self.videos[index]
        info = {}
        info['name'] = video
        info['num_frames'] = self.num_frames[video]
        start, end = self.index_map[video]
        info['start'] = start
        imgs = np.empty((self.num_frames[video],) + self.shape[video] + (3,), dtype=np.float32)
        N_frames = np.empty((self.num_frames[video],) + self.shape[video] + (3,), dtype=np.float32)
        N_masks = np.empty((self.num_frames[video],) + self.shape[video], dtype=np.uint8)
        for f in range(self.num_frames[video]):
            img_file = os.path.join(self.image_dir, '{:05d}.png'.format(f + start))
            imgs[f] = np.array(Image.open(img_file).convert('RGB'))
            imgs[f][np.where(imgs[f] < self.lowerbound)] = self.lowerbound
            imgs[f][np.where(imgs[f] > self.upperbound)] = self.upperbound
            imgs[f] = cv2.normalize(imgs[f], dst=None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
            N_frames[f] = imgs[f] / 255.
            try:
                mask_file = os.path.join(self.mask_dir, '{:05d}.png'.format(f + start))
                N_masks[f] = np.array(Image.open(mask_file).convert('P'), dtype=np.uint8)
            except:
                N_masks[f] = 255
        info['imgs'] = imgs
        Fs = torch.from_numpy(np.transpose(N_frames.copy(), (3, 0, 1, 2)).copy()).float()
        if self.single_object:
            N_masks = (N_masks > 0.5).astype(np.uint8) * (N_masks <= 255).astype(np.uint8)
            Ms = torch.from_numpy(self.All_to_onehot(N_masks).copy()).float()
            num_objects = torch.LongTensor([int(1)])
            return Fs, Ms, num_objects, info
        else:
            Ms = torch.from_numpy(self.All_to_onehot(N_masks).copy()).float()
            num_objects = torch.LongTensor([int(self.num_objects[video])])
            return Fs, Ms, num_objects, info


if __name__ == '__main__':
    img = cv2.imread('../output/agv_top_yellow/STMOutputImages/00000.png', 0)
    print(np.unique(img))
