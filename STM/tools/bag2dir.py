import os
import numpy as np
import cv2
import pickle


def func_bag2dir(args):
    import shutil
    from ctypes import cdll
    lib1 = cdll.LoadLibrary('/opt/ros/noetic/lib/libcv_bridge.so')
    from cv_bridge import CvBridge

    bag_f = args.bag_file
    assert os.path.exists(bag_f)

    name = os.path.splitext(os.path.split(bag_f)[-1])[0]

    # 1 Resolve bag file and extract depthth/color images with time stamp.
    out_color = os.path.join('data', name, 'color')  # color path
    out_depth = os.path.join('data', name, 'depth')  # depthth path
    shutil.rmtree(out_depth, ignore_errors=True)
    shutil.rmtree(out_color, ignore_errors=True)
    os.makedirs(out_color, exist_ok=True)
    os.makedirs(out_depth, exist_ok=True)

    color_stamp_f = open(os.path.join('data', name, 'color-stamp.txt'), 'w')  # time stamp for depth
    depth_stamp_f = open(os.path.join('data', name, 'depth-stamp.txt'), 'w')  # time stamp for color

    bridge = CvBridge()

    import rosbag
    with rosbag.Bag(bag_f, 'r') as bag:
        color_ci, depth_ci = None, None
        for topic, msg, t in bag.read_messages():
            timestr = "%.6f" % msg.header.stamp.to_sec()  # rgb time stamp
            if topic == "/camera/d435_color/image_raw":
                # Color Image
                cv_image = bridge.imgmsg_to_cv2(msg, "bgr8")
                image_name = timestr + ".png"
                color_stamp_f.write(timestr + '\n')
                cv2.imwrite(os.path.join(out_color, image_name), cv_image)

            if topic == "/camera/d435_color/camera_info":
                color_fx = msg.K[0]
                color_fy = msg.K[4]
                color_cx = msg.K[2]
                color_cy = msg.K[5]
                color_ci = msg

            if topic == "/camera/d435_depth/image_raw":
                # Image
                cv_image = bridge.imgmsg_to_cv2(msg)
                image_name = timestr + ".png"
                depth_stamp_f.write(timestr + '\n')
                cv2.imwrite(os.path.join(out_depth, image_name), cv_image)

            if topic == "/camera/d435_depth/camera_info":
                depth_fx = msg.K[0]
                depth_fy = msg.K[4]
                depth_cx = msg.K[2]
                depth_cy = msg.K[5]
                depth_ci = msg


        color_ci = {k: getattr(color_ci, k) for k in
                    ["height", "width", "distortion_model", "D", "K", "R", "P", "binning_x", "binning_y"]}
        depth_ci = {k: getattr(depth_ci, k) for k in
                    ["height", "width", "distortion_model", "D", "K", "R", "P", "binning_x", "binning_y"]}
        with open(os.path.join('data', name, "color_camerainfo.pkl"), "wb") as f:
            pickle.dump(color_ci, f)
        with open(os.path.join('data', name, "depth_camerainfo.pkl"), "wb") as f:
            pickle.dump(depth_ci, f)
        print(color_fx, color_fy, color_cx, color_cy)
        print(depth_fx, depth_fy, depth_cx, depth_cy)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("bag_file", type=str)
    parser.set_defaults(func=func_bag2dir)
    args = parser.parse_args()
    args.func(args)
