from __future__ import division

import cv2
import torch

from torch.utils import data

import torch.nn as nn
import torch.nn.functional as F

from PIL import Image
import numpy as np

import tqdm
import os
import argparse

from tools.dataset import DAVIS_MO_Test
from tools.model import STM
from tools.labelme2voc import labels_file

torch.set_grad_enabled(False)  # Volatile

file = open('./labels.txt', 'r')
for line in file:
    content = line.replace('\n', '')
    if len(content) > 0:
        labels_file.append(content)


def get_arguments():
    parser = argparse.ArgumentParser(description="Video Object Segmentation using Space-Time Memory Networks")
    parser.add_argument("-g", type=str, help="0; 0,1; 0,3; etc", default='0')
    parser.add_argument("-d", type=str, help="path to data", required=True)
    parser.add_argument("-o", type=str, help="object type", required=True)
    parser.add_argument("-l", type=int, help="image normalize lowerbound", default=0)
    parser.add_argument("-u", type=int, help="image normalize upperbound", default=255)
    return parser.parse_args()


args = get_arguments()

GPU = args.g
DATA_ROOT = args.d
ObJ = args.o
if ObJ not in labels_file:
    raise Exception('object type is incorrect.')
# Model and version
MODEL = 'STM'
print(MODEL, ': Testing on DAVIS')
#
os.environ['CUDA_VISIBLE_DEVICES'] = GPU
if torch.cuda.is_available():
    print('using Cuda devices, num:', torch.cuda.device_count())


def Run_video(Fs, Ms, num_frames, num_objects, Mem_every=None, Mem_number=None):
    # initialize storage tensors
    if Mem_every:
        to_memorize = [int(i) for i in np.arange(0, num_frames, step=Mem_every)]
    elif Mem_number:
        to_memorize = [int(round(i)) for i in np.linspace(0, num_frames, num=Mem_number + 2)[:-1]]
    else:
        raise NotImplementedError

    Es = torch.zeros_like(Ms)
    Es[:, :, 0] = Ms[:, :, 0]

    for t in tqdm.tqdm(range(1, num_frames)):
        # memorize
        with torch.no_grad():
            prev_key, prev_value = model(Fs[:, :, t - 1], Es[:, :, t - 1], torch.tensor([num_objects]))

        if t - 1 == 0:  #
            this_keys, this_values = prev_key, prev_value  # only prev memory
        else:
            this_keys = torch.cat([keys, prev_key], dim=3)
            this_values = torch.cat([values, prev_value], dim=3)

        # segment
        with torch.no_grad():
            logit = model(Fs[:, :, t], this_keys, this_values, torch.tensor([num_objects]))
        Es[:, :, t] = F.softmax(logit, dim=1)

        # update
        if t - 1 in to_memorize:
            keys, values = this_keys, this_values

    pred = np.argmax(Es[0].cpu().numpy(), axis=0).astype(np.uint8)
    return pred, Es


Testset = DAVIS_MO_Test(DATA_ROOT, single_object=True)
Testloader = data.DataLoader(Testset, batch_size=1, shuffle=False, num_workers=0, pin_memory=True)

model = nn.DataParallel(STM())
if torch.cuda.is_available():
    model.cuda()
model.eval()  # turn-off BN

pth_path = 'tools/STM_weights.pth'
print('Loading weights:', pth_path)
model.load_state_dict(torch.load(pth_path, map_location='cpu'))

code_name = '{}_val'.format(MODEL)
print('Start Testing:', code_name)

for seq, V in enumerate(Testloader):
    Fs, Ms, num_objects, info = V
    seq_name = info['name'][0]
    start = info['start']
    imgs = info['imgs']
    num_frames = info['num_frames'][0].item()
    print('[{}]: num_frames: {}, num_objects: {}'.format(seq_name, num_frames, num_objects[0][0]))

    pred, Es = Run_video(Fs, Ms, num_frames, num_objects, Mem_every=5, Mem_number=None)

    # Save results for quantitative eval ######################
    test_path = os.path.join(DATA_ROOT, 'STMOutputImages')
    imgviz_path = os.path.join(DATA_ROOT, 'SegmentationClassVisualization')
    if not os.path.exists(test_path):
        os.makedirs(test_path)
    print('Images Segmentation Result Visualization Saved in {}'.format(imgviz_path))
    for f in tqdm.tqdm(range(num_frames)):
        pred[f][np.where(pred[f] == 1)] = labels_file.index(ObJ) - 1
        img_E = Image.fromarray(pred[f])
        img_E.save(os.path.join(test_path, '{:05d}.png'.format(f + int(start))))
        out_viz_file = os.path.join(imgviz_path, '{:05d}.png'.format(f + int(start)))
        import imgviz

        viz = imgviz.label2rgb(
            label=np.array(img_E),
            image=np.array(imgs[0][f]),
        )
        imgviz.io.imsave(out_viz_file, viz)

path = os.path.join(DATA_ROOT, 'SegmentationClassVisualization')
filelist = os.listdir(path)
filelist = sorted(filelist)

fps = 24  # 视频每秒24帧
size = (640, 480)  # 需要转为视频的图片的尺寸
# 可以使用cv2.resize()进行修改
fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
video = cv2.VideoWriter(os.path.join(DATA_ROOT, "vis.mp4"), fourcc, fps, size)  # 创建.avi
# 视频保存在当前目录下

for item in filelist:
    if item.endswith('.png'):
        # 找到路径中所有后缀名为.jpeg的图片
        item = os.path.join(path, item)
        img = cv2.imread(item)
        video.write(img)

video.release()
