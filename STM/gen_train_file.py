import glob
import os
import argparse
import random


def get_arguments():
    parser = argparse.ArgumentParser(description="generate segmentation train file from input dir")
    parser.add_argument("input_dir", type=str, help="path to data")
    parser.add_argument("-l", type=int, help="data length ", default=100)
    parser.add_argument("-r", type=float, help="train/val ratio", default=0.8)
    return parser.parse_args()


def gen_file(file_list, output_file):
    for file in file_list:
        file = file[2:]
        img = file
        seg = file.replace('JPEGImages', 'STMOutputImages')
        output_file.write(img + ',' + seg + '\n')


root = os.getcwd()
args = get_arguments()
DIR = args.input_dir
LEN = args.l
RATIO = args.r
dirs = os.listdir(DIR)
train_file = open(os.path.join(DIR, 'train.txt'), 'w')
val_file = open(os.path.join(DIR, 'val.txt'), 'w')
train_list = []
val_list = []
for d in dirs:
    images_path = os.path.join(DIR, d, 'JPEGImages')
    images = glob.glob(os.path.join(images_path, '*.png'))
    random.shuffle(images)
    reserve_len = len(images) if len(images) < LEN else LEN
    images = images[:reserve_len]
    mid = int(RATIO * len(images))
    train_list.extend(images)
    val_list.extend(images[mid:])
gen_file(train_list, train_file)
gen_file(val_list, val_file)
